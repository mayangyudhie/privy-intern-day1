/// <reference types="cypress" />

describe('Test Website Codedamn', () => {

    beforeEach(() => {
      cy.visit('https://codedamn.com')
    })
  
    it('Test 1', () => {
      cy.contains('Learn Programming').should('exist')
      cy.contains('Build projects, practice and learn to code from scratch - without leaving your browser.').should('exist')
      cy.log('expected contents found, website loaded')
      cy.pause()
    })
  })
  